package com.recruitment.task.service;
import com.recruitment.task.dto.CurrencyStatsDto;
import com.recruitment.task.mapper.CurrencyStatsRawToCurrencyStatsDtoMapper;
import com.recruitment.task.websocket.client.CoinbaseWebSocketClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ApplicationService {

    private final CoinbaseWebSocketClient coinbaseWebSocketClient;
    private final CurrencyStatsRawToCurrencyStatsDtoMapper currencyStatsRawToCurrencyStatsDtoMapper;

    public List<CurrencyStatsDto> getCurrencyStats() {

        return coinbaseWebSocketClient.getResult().values()
                .stream()
                .map(currencyStatsRawToCurrencyStatsDtoMapper::mapToCurrencyStatsDto).collect(Collectors.toList());
    }

}
