package com.recruitment.task.mapper;

import com.recruitment.task.dto.CurrencyStatsDto;
import com.recruitment.task.dto.CurrencyStatsRaw;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
@DecoratedWith(CurrencyStatsRawToCurrencyStatsDtoMapperDecorator.class)
public interface CurrencyStatsRawToCurrencyStatsDtoMapper {

    @Mapping(source = "best_bid", target = "bid")
    @Mapping(source = "best_ask", target = "ask")
    @Mapping(source = "price", target = "last")
    CurrencyStatsDto mapToCurrencyStatsDto(CurrencyStatsRaw currencyStatsRaw);
}
