package com.recruitment.task.mapper;

import com.recruitment.task.dto.CurrencyStatsDto;
import com.recruitment.task.dto.CurrencyStatsRaw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class CurrencyStatsRawToCurrencyStatsDtoMapperDecorator
        implements CurrencyStatsRawToCurrencyStatsDtoMapper {

    @Autowired
    @Qualifier("delegate")
    protected CurrencyStatsRawToCurrencyStatsDtoMapper delegate;

    @Override
    public CurrencyStatsDto mapToCurrencyStatsDto(CurrencyStatsRaw currencyStatsRaw) {
        CurrencyStatsDto currencyStatsDto = delegate.mapToCurrencyStatsDto(currencyStatsRaw);


        currencyStatsDto.setInstrument(currencyStatsRaw.getProduct_id().replace("-",""));
        currencyStatsDto.setTime(LocalDateTime.parse(currencyStatsRaw.getTime(),
                DateTimeFormatter.ISO_DATE_TIME).format(DateTimeFormatter.ofPattern("HH:mm:ss")));

        return currencyStatsDto;
    }
}
