package com.recruitment.task.controller;

import com.recruitment.task.dto.CurrencyStatsDto;
import com.recruitment.task.service.ApplicationService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/currency")
public class ApplicationController {

    private ApplicationService applicationService;

    public ApplicationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @RequestMapping(value="/current", method = RequestMethod.GET)
    public List<CurrencyStatsDto> getCurrencyStats() {
        return applicationService.getCurrencyStats();
    }

}

