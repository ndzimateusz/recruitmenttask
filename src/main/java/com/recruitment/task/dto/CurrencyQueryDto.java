package com.recruitment.task.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CurrencyQueryDto {

    private String type;

    private List<String> product_ids;

    private List<String> channels;
}
