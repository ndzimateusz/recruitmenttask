package com.recruitment.task.dto;

import lombok.Data;

@Data
public class CurrencyStatsDto {

    private String instrument;

    private double bid;

    private double ask;

    private double last;

    private String time;
}
