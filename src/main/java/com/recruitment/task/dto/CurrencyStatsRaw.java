package com.recruitment.task.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CurrencyStatsRaw {
    private String type;

    private long sequence;

    private String product_id;

    @JsonFormat(shape=JsonFormat.Shape.STRING)
    private BigDecimal price;

    private double open_24h;

    private double volume_24h;

    private double low_24h;

    private double high_24h;

    private double volume_30d;

    @JsonFormat(shape=JsonFormat.Shape.STRING)
    private BigDecimal best_bid;

    @JsonFormat(shape=JsonFormat.Shape.STRING)
    private BigDecimal best_ask;

    private String side;

    private String time;

    private long trade_id;

    private double last_size;
}
