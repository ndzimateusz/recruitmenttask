package com.recruitment.task.websocket.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.recruitment.task.dto.CurrencyQueryDto;
import com.recruitment.task.dto.CurrencyStatsRaw;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

@Component
public class CoinbaseWebSocketClient extends TextWebSocketHandler implements
        ApplicationListener<ContextRefreshedEvent> {

    private static String URL = "wss://ws-feed.pro.coinbase.com";

    private static Logger logger = LogManager.getLogger(CoinbaseWebSocketClient.class);

    @Getter
    private HashMap<String, CurrencyStatsRaw> result;

    private ObjectMapper objectMapper;

    @Getter
    private WebSocketSession clientSession;

    public CoinbaseWebSocketClient() throws ExecutionException, InterruptedException {
        result = new HashMap();
        objectMapper = new ObjectMapper();
        WebSocketClient webSocketClient = new StandardWebSocketClient();
        this.clientSession = webSocketClient.doHandshake(this, new WebSocketHttpHeaders(), URI.create(URL)).get();
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) {
        try {
            CurrencyStatsRaw value = objectMapper.readValue(message.getPayload(), CurrencyStatsRaw.class);
            result.put(value.getProduct_id(), value);
        } catch (JsonProcessingException e) {
            logger.error("not a currency");
        }
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        try {
            this.getClientSession().sendMessage(new TextMessage(objectMapper.writeValueAsString(CurrencyQueryDto.builder()
                    .type("subscribe")
                    .product_ids(Arrays.asList("ETH-USD", "ETH-EUR", "BTC-EUR", "BTC-USD"))
                    .channels(Arrays.asList("ticker"))
                    .build())));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    @PreDestroy
    public void onDestroy() {
        try {
            clientSession.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}